const mongoose = require('mongoose'); // Import de la librairie mongoose
const Schema = mongoose.Schema;

// Définition du schéma
const ContactItemSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true}
  }, 
  {timestamps: true} // Pour avoir les dates de création et de modification automatiquement gérés par mongoose
);

module.exports = ContactItemSchema; // Export du schéma