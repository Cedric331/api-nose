const ContactListController = require('../controller/ContactList'); // Import du contrôleur

module.exports = (app) => {
  app.route('/contact').get(ContactListController.getAll);
  app.route('/contact').post(ContactListController.create);
  app.route('/contact/:id').get(ContactListController.get);
  app.route('/contact/:id').put(ContactListController.update);
  app.route('/contact/:id').delete(ContactListController.delete);

  app.use((req, res) => { // Middleware pour capturer une requête qui ne match aucune des routes définies plus tôt
    res.status(404).json({url: req.originalUrl, error: 'Erreur de requête'});
  });
};