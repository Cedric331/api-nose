const contactItem = require('../model/ContactItem'); // On en a déjà parlé, vous vous en rappelez?
const mongoose = require('mongoose'); // Import du schéma
const ContactItem = mongoose.model('ContactItem', contactItem); // Création du modèle à partir du schéma

function respond(err, result, res) { // Fonction utilisée tout au long du contrôleur pour répondre
  if (err) {
    return res.status(500).json({error: err});
  }
  return res.json(result);
}

const ContactListController = 
{
  getAll: (req, res) => {  // Récupérer tous les items de la ContactList
    ContactItem.find({}, (err, contactItem) => {
      return respond(err, contactItem, res);
    });
  },

  create: (req, res) => { // Créer une tâche
    const newContactItem = new ContactItem(req.body);
    newContactItem.save((err, savedContactItem) => {
      return respond(err, savedContactItem, res);
    });
  },

  get: (req, res) => { // Récupérer une tâche
    ContactItem.findById(req.params.id, (err, contactItem) => {
      return respond(err, contactItem, res);
    });
  },

  update: (req, res) => { // Mettre à jour une tâche
   ContactItem.findOneAndUpdate(req.params.id, req.body, (err, contactItem) => {
     return respond(err, contactItem, res);
   });
 },

  delete: (req, res) => { // Supprimer une tâche
    ContactItem.findByIdAndRemove(req.params.id, (err, contactItem) => {
      return respond(err, contactItem, res);
    });
  }

};

module.exports = ContactListController; // Export du contrôleur